const express = require('express')
require('./db/mongoose')

const userRouter = require('./routers/user')
const taskRouter = require('./routers/task')
const categoryRouter = require('./routers/category')
const productRouter = require('./routers/product')
const orderRouter = require('./routers/order')
const app = express()
const port = process.env.PORT 
const multer = require('multer')
const upload = multer({
    dest: 'images'
})

app.post('/upload', upload.single('upload'), (req, res) => {
    res.send()
})

app.use(express.json())
app.use(userRouter)
app.use(taskRouter)
app.use(categoryRouter)
app.use(productRouter)
app.use(orderRouter)

app.listen(port, () => {
    console.log('Server is up on port: ' + port)
})

