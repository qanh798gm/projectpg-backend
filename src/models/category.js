const mongoose = require('mongoose')

const categorySchema = mongoose.Schema({
    name: {
        type: String,
        required: true, 
        trim: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
    }
}, {
    timestamps: true
})

categorySchema.virtual('products', {
    ref: 'Product',
    localField: '_id',
    foreignField: 'categoryID'
})

const Category = mongoose.model('Category', categorySchema)

module.exports = Category