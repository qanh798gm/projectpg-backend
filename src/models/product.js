const mongoose = require('mongoose')

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true, 
        trim: true
    },
    brand: {
        type: String,
        required: true, 
        trim: true
    },
    price: {
        type: Number,
        required: true, 
        trim: true
    },
    description: {
        type: String,
        required: true, 
        trim: true
    },
    image: {
        type: Buffer
    },
    categoryID: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Category'
    }
}, {
    timestamps: true
})

const Product = mongoose.model('Product', productSchema)

module.exports = Product