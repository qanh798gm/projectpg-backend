const mongoose = require('mongoose')

const orderSchema = mongoose.Schema({
    product: [{
        _id: {
            type: String
        },
        quantity: {
            type: Number
        }
    }],
    totalPrice: {
        type: Number
    },
    detail: {
        address: {
            type: String
        },
        note: {
            type: String
        }
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
}, {
        timestamps: true
    })


const Order = mongoose.model('Order', orderSchema)

module.exports = Order