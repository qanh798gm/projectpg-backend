const express = require('express')
const router = new express.Router()
const User = require('../models/user')
const auth = require('../middleware/auth')
const multer = require('multer')
const sharp = require('sharp')
const mongoose = require('mongoose')
const Product = require('../models/product')

const { sendWelcomeEmail, sendSorryEmail } = require('../emails/account')
//upload type
const upload = multer({
  limits: {
    fileSize: 1000000
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error('Please up load a picture file.'))
    }
    cb(undefined, true)
  }
})

//Add user
router.post('/users', async (req, res) => {
  const user = new User(req.body)
  try {
    await user.save()
    //sendWelcomeEmail(user.email, user.name)
    const token = await user.generateAuthentication()
    res.status(201).send({ user, token })
  } catch (e) {
    res.status(400).send(e)
  }
})

//Login
router.post('/users/login', async (req, res) => {
  try {
    const user = await User.findByCredentials(req.body.email, req.body.password)
    const token = await user.generateAuthentication()
    res.send({ user, token })
  } catch (e) {
    res.status(400).send(e)
  }
})

//Logout
router.post('/users/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token
    })

    req.user.save()
    res.send()
  } catch (e) {
    res.status(500).send()
  }
})

//Logout all tokens
router.post('/users/logoutAll', auth, async (req, res) => {
  try {
    req.user.tokens = []
    req.user.save()
    res.send()
  } catch (e) {
    res.status(500).send()
  }
})

//See profile
router.get('/users/me', auth, async (req, res) => {
  res.send(req.user)
})

//Update profile
router.patch('/users/me', auth, async (req, res) => {
  const updates = Object.keys(req.body)
  const allowedUpdates = ['name', 'email', 'password', 'age', 'type', 'cart']
  const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
  if (!isValidOperation) {
    res.status(400).send({ error: 'Invalid updates!' })
  }
  try {
    updates.forEach((update) => req.user[update] = req.body[update])
    await req.user.save()

    res.send(req.user)
  } catch (e) {
    res.status(400).send(e)
  }
})

//Delete user
router.delete('/users/me', auth, async (req, res) => {
  try {
    await req.user.remove()
    //sendSorryEmail(req.user.email, req.user.name)
    res.send(req.user)
  } catch (e) {
    res.status(500).send(e)
  }
})

//Update avatar
router.post('/users/me/avatar', auth, upload.single('avatar'), async (req, res) => {
  const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
  req.user.avatar = buffer
  await req.user.save()
  res.send()
}, (error, req, res, next) => {
  res.status(400).send({ error: error.message })
})

//Delete avatar
router.delete('/users/me/avatar', auth, async (req, res) => {
  try {
    req.user.avatar = undefined
    await req.user.save()
    res.send()
  } catch (e) {
    res.status(400).send(e)
  }

})

//Read user avatar
router.get('/users/:id/avatar', async (req, res) => {
  try {
    const user = await User.findById(req.params.id)
    if (!user || !user.avatar) {
      throw new Error
    }
    res.set('Content-Type', 'image/png')
    res.send(user.avatar)
  } catch (e) {
    res.status(404).send
  }
})

router.post('/users/addCart', auth, async (req, res) => {
  let duplicate = false;
  req.user.cart.forEach(item => {
    if (item._id == req.body._id) {
      console.log(item._id)
      return duplicate = true
    }
  })

  if (!duplicate) {
    let pushCart = {
      _id: mongoose.Types.ObjectId(req.body._id),
      quantity: req.body.quantity ? req.body.quantity : 1,
      price: req.body.price,
      date: Date.now()
    }
    //use findOne instead of fineOneAndUpdate to avoid duplicate
    await User.findOne(
      { _id: req.user.id },
      (err, user) => {
        if (err) return res.json({ success: false, err })
        user.cart.push(pushCart)
        user.save()
        let cart = user.cart;
        let array = cart.map(item => {
          return mongoose.Types.ObjectId(item.id);
        })
        //return product cart detail
        Product.find({ _id: { $in: array } })
          .exec((err, cartDetail) => {
            return res.status(200).json({
              cartDetail,
              cart
            })
          })
      }
    )
    console.log('create new');
  }
  else {
    const updateCart = req.body.quantity ? req.body.quantity : 1
    console.log(updateCart)
    try {
      const updatedUser = await User.findOneAndUpdate(
        {
          _id: req.user.id,
          "cart._id": mongoose.Types.ObjectId(req.body._id)
        },
        {
          $inc: { "cart.$.quantity": updateCart }
        },
        { new: true },
        (err, user) => {
          if (err) return res.json({ success: false, err })
          let cart = user.cart;
          let array = cart.map(item => {
            return mongoose.Types.ObjectId(item.id);
          });
          Product.find({ _id: { $in: array } })
            .exec((err, cartDetail) => {
              return res.status(200).json({
                cartDetail,
                cart
              })
            })
        }
      )
      updatedUser.save()

    } catch (e) {
      res.status(404).send(e)
    }


    // if (err) return res.json({ success: false, err })
    // let cart = user.cart;
    // let array = cart.map(item => {
    //   return mongoose.Types.ObjectId(item._id);
    // });

    // Product.find({ _id: { $in: array } })
    //   .exec((err, cartDetail) => {
    //     return res.status(200).json({
    //       cartDetail,
    //       cart
    //     })
    //   })
  }

  console.log('update');
}
)
module.exports = router

// router.post('/users/addCart/:id', auth, async (req, res) => {
//   console.log(req.params.id)
//   console.log(req.user.id);
//   console.log(req.user.cart);
//   let duplicate = false;
//   req.user.cart.forEach(item => {
//     if (item.id == req.params.id) {
//       duplicate = true;
//     }
//   })


//   if (duplicate) {
//     const updatedUser = await User.findOneAndUpdate(
//       {
//         _id: req.user.id,
//         "cart.id": mongoose.Types.ObjectId(req.body._id)
//       },
//       {
//         $inc: { "cart.$.quantity": req.body.quantity ? parseInt(req.body.quantity) : 1 }
//       },
//       { new: true },
//       (err, user) => {
//         if (err) return res.json({ success: false, err })
//         let cart = user.cart;
//         let array = cart.map(item => {
//           return mongoose.Types.ObjectId(item.id);
//         });
//         Product.find({ _id: { $in: array } })
//           .exec((err, cartDetail) => {
//             return res.status(200).json({
//               cartDetail,
//               cart
//             })
//           })
//       }
//     )
//     updatedUser.save()
//   } else {
//     const updatedUser = await User.findOneAndUpdate(
//       { _id: req.user.id },
//       {
//         $push: {
//           cart: {
//             id: mongoose.Types.ObjectId(req.params.id),
//             quantity: req.body.quantity ? req.body.quantity : 1,
//             price: mongoose.Types.ObjectId(req.query.price),
//             date: Date.now()
//           }
//         }
//       },
//       { new: true },
//       (err, user) => {
//         if (err) return res.json({ success: false, err })
//         let cart = user.cart;
//         let array = cart.map(item => {
//           return mongoose.Types.ObjectId(item.id);
//         })
//         Product.find({ _id: { $in: array } })
//           .exec((err, cartDetail) => {
//             return res.status(200).json({
//               cartDetail,
//               cart
//             })
//           })
//       }
//     )
//     updatedUser.save()
//   }
// })