const express = require('express')
const router = new express.Router()
const Order = require('../models/order')
const auth = require('../middleware/auth')

//Add order
router.post('/orders', auth, async (req, res) => {
    const order = new Order(req.body)
    try {
        await order.save()
        res.status(201).send(order)
    } catch (e) {
        res.status(400).send(e)
    }
})

//find 1 order detail
router.get('/order-detail/:id', auth, async (req, res) => {
    const _id = req.params.id
    try {
        const order = await Order.findById(_id)
        if (!order) {
            return res.status(404).send(e)
        }
        res.send(order)
    } catch (e) {
        res.status(500).send(e)
    }
})

//get all order
router.get('/orders', auth, async (req, res) => {
    try {
        const orders = await Order.find({ })
        if (!orders) {
            return res.status(404).send(e)
        }
        res.send(orders)
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router