const express = require('express')
const router = new express.Router()
const Product = require('../models/product')
const auth = require('../middleware/auth')
const multer = require('multer')
const sharp = require('sharp')

//upload type
const upload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error('Please up load a picture file.'))
        }
        cb(undefined, true)
    }
})

//Add product with image
router.post('/products', upload.single('image'), async (req, res) => {
    const product = new Product(req.body)
    const buffer = await sharp(req.file.buffer).resize({ width: 250, height: 250 }).png().toBuffer()
    try {
        product.image = buffer
        await product.save()
        res.status(201).send(product)
    } catch (e) {
        res.status(400).send(e)
    }
})


//find 1 product detail
router.get('/product-detail/:id', async (req, res) => {
    const _id = req.params.id
    try {
        const product = await Product.findById(_id)
        if (!product) {
            return res.status(404).send(e)
        }
        res.send(product)
    } catch (e) {
        res.status(500).send(e)
    }
})

//get all products
router.get('/products', async (req, res) => {
    try {
        const products = await Product.find({})
        if (!products) {
            return res.status(404).send(e)
        }
        res.send(products)
    } catch (e) {
        res.status(500).send(e)
    }
})

//find product by category id
router.get('/products/:categoryID', async (req, res) => {
    console.log(req.params.categoryID);
    try {
        const products = await Product.find({ categoryID: req.params.categoryID })
        if (!products) {
            return res.status(404).send(e)
        }
        res.send(products)
    } catch (e) {
        res.status(500).send(e)
    }
})

router.patch('/products/:id', auth, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['description', 'completed']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        res.status(400).send({ error: 'Invalid update!' })
    }

    try {
        const product = await Product.findOne({ _id: req.params.id, owner: req.user._id })

        if (!task) {
            return res.status(404).send()
        }

        updates.forEach((update) => product[update] = req.body[update])
        task.save()
        res.send(product)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/products/:id', auth, async (req, res) => {
    try {
        const product = await Product.findOneAndDelete({ _id: req.params.id, owner: req.user._id })
        if (!product) {
            return res.status(404).send()
        }
        res.send(product)
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router